﻿using System.Diagnostics;
using System.Security;

namespace RunAsApp
{
	class Program
	{
		static void Main(string[] args)
		{
			Process proc = new Process();
			SecureString ssPwd = new SecureString();
			proc.StartInfo.UseShellExecute = false;
			proc.StartInfo.FileName = "filename";
			proc.StartInfo.Domain = "domainname";
			proc.StartInfo.UserName = "username";
			string password = "password";
			for (int x = 0; x < password.Length; x++)
			{
				ssPwd.AppendChar(password[x]);
			}
			password = "";
			proc.StartInfo.Password = ssPwd;
			proc.Start();
		}
	}
}
